Name: Synthesis of HAp from Fish Scales
Interactive Simulation

**Tech stack used:**
Front End
- HTML 5
- CSS 3
- JavaScript

NOTE: 
i. No databases or server-side scripting was used in this project
ii. As minimal CSS is used, it is written in HTML file head element itself between style tags

**Dependencies**

JS Libraries:
- Jquery 3.5.1
- CreateJS 1.0.0

**Directory structure**

```bash
HAp/
├─ assets/
│  │  ├─ fractured.png
│  │  ├─ catla.png
│  │  ├─ furnace.png
│  │  ├─ oven_door.png
│  │  ├─ oven_knob_turned.png
│  │  ├─ nacl_cap.png
│  │  ├─ raw_fish_scales.png
│  │  ├─ nacl_solution.png
│  │  ├─ mortar_powder.png
│  │  ├─ HAp_molecular.png
│  │  ├─ oven_knob.png
│  │  ├─ mortar_granules.png
│  │  ├─ beaker_fishscales.png
│  │  ├─ grafted.png
│  │  ├─ fish_scales.png
│  │  ├─ furnace_door.png
│  │  ├─ pestle.png
│  │  ├─ nacl_bottle_fulltilt.png
│  │  ├─ arrow.png
│  │  ├─ nacl_solution_toomuch.png
│  │  ├─ crystalline.png
│  │  ├─ nacl_bottle.png
│  │  ├─ hap_granules.png
│  │  ├─ hap_powder.png
│  │  ├─ oven.png
├─ lib/
│  ├─ scenes/
│  │  ├─ scene_0.js
│  │  ├─ scene_1.js
│  │  ├─ scene_2.js
│  │  ├─ scene_3.js
│  │  ├─ scene_4.js
│  │  ├─ scene_5.js
│  ├─ createjs.min.1.0.0,js
│  ├─ jquery.min.3.5.1.js
│  ├─ stageshow.0.1.js
├─ index.html
```

**Functionality of important files and scripts**

The working directory contains the following subdirectories:

- `./0-app/` is the directory in which the app will be hosted. This may be a sub-directory under the [webroot] of the site (usually "public_html" or "www" on Apapche) and it is a shortened version of the name of the app (i.e. "HAp"). This contains the landing page of the app called "index.html" and two directories called "assets" and "lib".
- `./1-assets/` directory contains all graphic images used in the app in PNG or JPG formnat.
- `./2-lib/` directory contains all the JS files including minimized third party JS libraries (jquery and createjs) as well as two other directories "components" and "scenes". Additionally the main framework js file is stored as "stageshow.0.1.js" in this directory.
- `./4-scenes` is the directory in which each scene controller is stored. The naming convention of each scene controller is scene_[number].js. The app reads and loads them sorted by ascending number on runtime.

**Workflow**

1. All graphic files are created and stored in the "assets" directory.
2. Landing view "index.html" is based upon common template with JS libs and scripts with runtime instructions on load event. Necessary changes may be made to title, style, etc.
3. Only required UI component object files are  created in the "lib/components" directory.4. Scene controllers are created as separate files in created in the "lib/scenes" directory.

**Routing and communication between files**

**Contributing instructions**

**Build/packaging instructions**

**Deployment/hosting Instructions**

If the app is hosted under its own TLD exclusively, then the contents of the [app] (i.e. "HAp") directory may be uploaded in the [webroot] directory itself (accessible e.g. https://[app_name].edu.in) and directory [app] (i.e. HAp) does not need to be created. Otherwise, if the app is hosted as a subdomain or linked by a path, the [app] directory may be uploaded to the [webroot] directory in which case it may be accessed by https://[app_name].[domain_name].edu.in (sub-domain) or https://[domain_name].edu.in/[app_name]/

    NOTE: While the name of the top level directory may be changed (i.e "HAp" --> "fish_scales"), it is advisable not to do so. All other directory names and file names are case-sensitive and their names and/or extensions must not be changed. If the initial landing page is to be changed, "index.html" maybe renamed and linked appropriately.

The app will only work in a server environment. It will not work locally unless a web server is running in the system (localhost) and it is hosted in its webroot. Since it does not use any server side scripts or databases, it can be hosted on any platform (*nix, MacOS, Windows NT).

**Security**

There are no known security issues.

All third party libraries are added ton each project elininating the need to call from other servers. As such, there is no chance of any XSS attacks.
